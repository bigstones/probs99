{-# LANGUAGE TemplateHaskell #-} -- this is to make $(quickCheckAll) work

module Probs46to50 where

-- ---------------------------------------------
-- http://www.haskell.org/haskellwiki/99_questions/46_to_50
-- ---------------------------------------------

import Test.QuickCheck
import Test.QuickCheck.All
import Data.List (intersperse)

-- ---------------------------------------
-- 46 (**) Define and, or, nand, nor, xor, impl and equ, then write
--         a predicate table/3 which prints the truth table of a given
--         logical expression in two variables.

and' :: Bool -> Bool -> Bool
and' = (&&)

or' :: Bool -> Bool -> Bool
or' = (||)

nand' :: Bool -> Bool -> Bool
nand' a b = not $ and' a b

nor' :: Bool -> Bool -> Bool
nor' a b = not $ or' a b

xor' :: Bool -> Bool -> Bool
xor' True False = True
xor' False True = True
xor' _ _ = False

impl' :: Bool -> Bool -> Bool
impl' a b = (not a) `or'` b

equ' :: Bool -> Bool -> Bool
equ' = (==)


table :: (Bool -> Bool -> Bool) -> IO ()
table f = do
    let params = [(a,b) | a <- [True, False], b <- [True, False]]
        results = map (\(a,b) -> (a, b, f a b)) params
        outputs = map prettify results
        prettify (a,b,c) = concat $ intersperse " " [show a, show b, show c]
    mapM_ putStrLn outputs

-- TODO: impure code needs hunit testing


-- ---------------------------------
-- 47 (*) Truth tables for logical expressions (2).

infixl 4 `or'`
infixl 6 `and'`

-- as per precendeces:
--logical not
--equality
--and
--xor
--or


-- ---------------------------------
-- 48 (**) Truth tables for logical expressions (3).

-- probably pretty unefficient
cartPow :: Integral a => a -> [b] -> [[b]]
cartPow 0 _  = []
cartPow 1 xs = [[x] | x <- xs ]
cartPow n xs = [xx ++ [x] | xx <- xxs, x <- xs]
    where xxs = cartPow (n-1) xs

-- not too pretty but who cares.
tablen :: Integral a => a -> ([Bool] -> Bool) -> IO ()
tablen n f = do
    let params = cartPow n [True, False]
        results = [p ++ [f p] | p <- params]
        outputs = map (\xs -> unwords $ map show xs) results
    mapM_ putStrLn outputs


-- ---------------------------------
-- 49 (**) Gray codes.

gray :: Integral a => a -> [String]
gray 0 = [""]
gray n = map ('0':) g ++ map ('1':) (reverse g)
    where g = gray (n-1)


-- --------------------------------
-- 50 (***) Huffman codes.

-- TODO: maybe too difficult. (or too lazy)



----------------------------

main :: IO Bool
main = $(quickCheckAll)
--main = quickCheck prop_03

