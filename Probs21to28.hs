{-# LANGUAGE TemplateHaskell #-} -- this is to make $(quickCheckAll) work

module Probs21to28 where

-- ---------------------------------------------
-- http://www.haskell.org/haskellwiki/99_questions/21_to_28
-- ---------------------------------------------

import Test.QuickCheck
import Test.QuickCheck.All
import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.Function as F
import Probs01to10 hiding (main)

-- ---------------------------------------
-- 21 Insert an element at a given position into a list.

insertAt :: a -> [a] -> Int -> [a]
--insertAt x xs p = before ++ x:after
--    where (before, after) = splitAt (p-1) xs

insertAt y []     _ = [y]
insertAt y (x:xs) p
    | p <= 1    = y:x:xs
    | otherwise = x:insertAt y xs (p-1)

prop_21 :: Char -> [Char] -> Int -> Bool
prop_21 x xs p = insertAt x xs p == before ++ x:after
    where (before, after) = splitAt (p-1) xs


-- --------------------------------------
-- 22 Create a list containing all integers within a given range.

range :: (Integral a) => a -> a -> [a]
range x y
    | x > y     = []
    | otherwise = x:range (x+1) y
    
-- FIXME: is deadly slow after 30-35 tests (numbers too big)
prop_22 :: Int -> Int -> Property
prop_22 x y =
    (-10000) < x && x < 10000 && (-10000) < y && y < 10000 ==>
    range x y == [x..y]


-- -------------------------------
-- 23 Extract a given number of randomly selected elements from a list.

-- TODO: skipped because randomicity is hard


-- --------------------------------
-- 24 Lotto: Draw N different random numbers from the set 1..M.

-- TODO: skipped because randomicity is hard


-- ---------------------------------
-- 25 Generate a random permutation of the elements of a list.

-- TODO: skipped because randomicity is hard


-- ----------------------------------
-- 26 (**) Generate the combinations of K distinct objects chosen
--         from the N elements of a list

combinations :: Int -> [a] -> [[a]]
--combinations 0 _  = [[]]
--combinations k xs = -- could have been clearer...
    --let tails' = [xs' | xs' <- List.tails xs, length xs' >= k]
        --helper (y:ys) = map (y:) (combinations (k-1) ys)
    --in  concatMap helper tails'

combinations 0 _  = [[]]
combinations k xs = -- because we can pattern match in list comprehensions!
    [ x:ys | x:xs' <- List.tails xs, length xs' >= k-1,
             ys <- combinations (k-1) xs' ]

prop_26 :: Bool
prop_26 = combinations 3 "abcde" ==
    ["abc","abd","abe","acd","ace","ade","bcd","bce","bde","cde"]


-- -------------------------
-- 27 Group the elements of a set into disjoint subsets.

-- TODO: too hard for me.


-- -------------------------
-- 28 Sorting a list of lists according to length of sublists,
--    then according to their length frequency.

lsort :: [[a]] -> [[a]]
lsort xxs = List.sortBy (\xs ys -> compare (length xs) (length ys)) xxs

-- this could be really simplified -- FIXME: it's actually broken.
lfsort :: [[a]] -> [[a]]
lfsort xxs = List.sortBy (\xs ys -> compare (lFreq xs) (lFreq ys)) xxs
    where lFreq zs = lFreqMap Map.! length zs
          lFreqMap = Map.fromList $ rleEncode $ List.sort $ map length xxs


prop_28_1 :: [[Int]] -> Bool
prop_28_1 xxs = lsort xxs == List.sortBy (compare `F.on` length) xxs

prop_28_2 :: [[Int]] -> Bool
prop_28_2 xxs = lfsort xxs == (concat . lsort . List.groupBy ((==) `F.on` length) . lsort) xxs

----------------------------

main :: IO Bool
main = $(quickCheckAll)
--main = quickCheck prop_03

