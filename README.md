# 66 Haskell Problems with QuickCheck tests

I'm learning Haskell (as I can't ride my bike) and solving the
[H-99 Haskell problems](http://www.haskell.org/haskellwiki/99_questions).

Being all this way too easy (yeah, sure), I decided to also write the tests
for my solutions.

Preferences in testing methods are:

1. test against library implementations
2. test for identity in case of inverse functions (`x == f(g(x))`)
3. test against some trivial combination of library functions
4. test against some smart solution to the problem (verified by checking
   solutions)
5. test for one input only (if QuickCheck gives up for too many discards)

The tests of case *5* are marked with a **TODO** in case someday I'll
try to fix them.

Have fun,

Andrea
