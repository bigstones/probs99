{-# LANGUAGE TemplateHaskell #-} -- this is to make $(quickCheckAll) work

module Probs11to20 where

-- ---------------------------------------------
-- http://www.haskell.org/haskellwiki/99_questions/11_to_20
-- ---------------------------------------------

import Test.QuickCheck
import Test.QuickCheck.All
import Data.List as List

import Probs01to10 (rleEncode, pack)

-- ---------------------------------------
-- 11 (*) Modified run-length encoding.

data ListItem a = Single a | Multiple Int a
    deriving (Show, Eq)

rleEncodeMod :: (Eq a) => [a] -> [ListItem a]
rleEncodeMod xs = map (\x -> listItem x) (pack xs)
    where
        listItem [y] = Single y
        listItem ys = Multiple (length ys) (head ys)

prop_11 :: (Eq a) => [a] -> Bool
prop_11 xs = rleEncodeMod xs == map encodeHelper (rleEncode xs)
    where
        encodeHelper (1,x) = Single x
        encodeHelper (n,x) = Multiple n x


-- -----------------------------------------
-- 12 (**) Decode a run-length encoded list (the one of prob 11)

--rleDecodeMod [] = []
--rleDecodeMod (x:xs) = (decodeHelper x) ++ (rleDecodeMod xs)

--rleDecodeMod = foldl (\acc x -> acc ++ decodeHelper x) []

rleDecodeMod :: [ListItem b] -> [b]
rleDecodeMod = concatMap decodeHelper
    where
        decodeHelper (Single x) = [x]
        decodeHelper (Multiple n x) = replicate n x

prop_12 :: Eq b => [b] -> Bool
prop_12 xs = (rleDecodeMod.rleEncodeMod) xs == xs


-- ----------------------------
-- 13 (**) Run-length encoding of a list (direct solution).

-- TODO: I don't get this problem.


-- ----------------------------
-- 14 (*) Duplicate the elements of a list.

dupli :: [a] -> [a]
--dupli = concatMap (\x -> replicate 2 x)

--dupli [] = []
--dupli (x:xs) = [x,x] ++ dupli xs

dupli = foldl (\acc x -> acc ++ replicate 2 x) []

prop_14 :: (Eq a) => [a] -> Bool
prop_14 xs = dupli xs == concatMap (\x -> replicate 2 x) xs


-- -----------------------------
-- 15 (**) Replicate the elements of a list a given number of times.

repli :: [a] -> Int -> [a]
repli xs n = concatMap (\x -> replicate n x) xs
-- as above, but 2 becomes a parameter.


-- TODO: this test gives up
--prop_15 xs n =
    --n > 0 && n < 1000 ==>
    --all (\(len,val) -> len `mod` n == 0) (rleEncode (repli xs n))
prop_15 :: Bool
prop_15 = repli ([1,2,3]::[Int]) 3 == [1,1,1,2,2,2,3,3,3]


-- ---------------------------
-- 16 (**) Drop every N'th element from a list.

dropEvery :: [a] -> Int -> [a]
dropEvery [] _ = []
dropEvery xs n = beforeN ++ dropEvery (afterN) n
    where
        beforeN = take (n-1) xs
        afterN = drop n xs

-- TODO: this test gives up
--prop_16 xs n =
    --0 < n && n < length xs ==>
    --(dropEvery xs n) == (map fst $ filter ((n/=) . snd) $ zip xs (cycle [1..n]))

prop_16 :: Bool
prop_16 = dropEvery "abcdefghik" 3 == "abdeghk"


-- ------------------------------
-- 17 (*) Split a list into two parts; the length of the first
--        part is given.

split :: [a] -> Int -> ([a],[a])
--split xs n = (take n xs, drop n xs)

split [] _      = ([],[])
split xs 0      = ([],xs)
split (x:xs) 1  = ([x],xs) -- optional, saves just one iteration
split (x:xs) n  = (x : fstRest, sndRest)
    where (fstRest, sndRest) = split xs (n-1)

-- TODO: this test gives up
--prop_17 xs n =
    --0 < n && n < length xs ==>
    --split xs n == List.splitAt n xs
prop_17 :: Bool
prop_17 = split "abcdefghik" 3 == ("abc", "defghik")


-- ---------------------------
-- 18 (**) Extract a slice from a list (idx start from 1)

slice :: [a] -> Int -> Int -> [a]
-- slice xs i k = take (k-i+1) $ drop (i-1) xs

-- needs boundary checks
--slice _      _ 0 = []
--slice (x:xs) 1 k = x : slice xs 1 (k-1)
--slice (_:xs) i k = slice xs (i-1) (k-1)

slice []     _ _ = []
slice (x:xs) i k
    | k < 1     = []
    | i <= 1    = x : slice xs 1 (k-1)
    | otherwise = slice xs (i-1) (k-1)

-- TODO: this test gives up
--prop_18 xs i k =
    --1 <= i && i <= k && k <= length xs ==> 
    --slice xs i k == (take (k-i+1) $ drop (i-1) xs)
prop_18 :: Bool
prop_18 = slice "abcdefghik" 3 7 == "cdefg"


-- -----------------------------
-- 19 (**) Rotate a list N places to the left. Accepts negative N.
--         (hint: use length and ++)

rotateL :: [a] -> Int -> [a]
rotateL [] _ = []
rotateL xs n = drop pos xs ++ take pos xs
    where
        pos = n `mod` (length xs)
        
prop_19 :: [Int] -> Int -> Bool
prop_19 xs n = (rotateL xs n) == (take len . drop (n `mod` len) . cycle $ xs)
    where len = length xs


-- ------------------------------
-- 20 (*) Remove the K'th element from a list.

removeAt :: Int -> [a] -> (a,[a])
--removeAt n xs = (remElem, purgedList)
    --where
        --remElem = xs !! (n-1)
        --purgedList = take (n-1) xs ++ drop n xs

removeAt _ []   = error "empty string"
removeAt n (x:xs)
    | (n <= 0 || n > (1 + length xs)) = error "index out of bounds"
    | n > 1     = let (remElem, purgedList) = removeAt (n-1) xs
                  in (remElem, x:purgedList)
    | n == 1    = (x, xs)

-- TODO make it with Maybe to avoid errors
    
    
-- TODO: this test would give up
prop_20 :: Bool
prop_20 = removeAt 2 "abcd" == ('b',"acd")

--------------------------

main :: IO Bool
main = $(quickCheckAll)
--main = quickCheck prop_03
