{-# LANGUAGE TemplateHaskell #-} -- this is to make $(quickCheckAll) work

module Probs31to41 where

-- ---------------------------------------------
-- http://www.haskell.org/haskellwiki/99_questions/31_to_41
-- ---------------------------------------------

import Test.QuickCheck
import Test.QuickCheck.All
import Data.List (group, genericLength)

import Probs01to10 (rleEncode)

-- ---------------------------------------
-- 31 (**) Determine whether a given integer number is prime.

isPrime :: (Integral a) => a -> Bool
isPrime 1 = False
--isPrime x = all (\d -> x `mod` d /= 0) [2..(x-1)] -- too naive

isPrime x = all (\d -> x `mod` d /= 0) [2..(floor . sqrt $ fromIntegral x)]

-- a faster implementation to speed up (?) the test
isPrime' :: (Integral a) => a -> Bool
isPrime' n | n < 4 = n /= 1 
isPrime' n = all ((/=0) . mod n) $ takeWhile (<= m) candidates 
        where candidates = (2:3:[x + i | x <- [6,12..], i <- [-1,1]])
              m = floor . sqrt $ fromIntegral n

prop_31 :: Int -> Bool
prop_31 x = isPrime x == isPrime' x


-- ----------------------------
-- 32 (**) Determine the greatest common divisor of two positive
--         integer numbers. Use Euclid's algorithm.

myGCD :: (Integral a) => a -> a -> a
myGCD x y
    | x == 0     = y'
    | y == 0     = x'
    | x' >= y'   = myGCD (x'-y') y'
    | otherwise  = myGCD x' (y'-x')
    where x' = abs x ; y' = abs y


prop_32 :: Int -> Int -> Bool
prop_32 x y = myGCD x y == gcd x y


-- ---------------------------
-- 33 (*) Determine whether two positive integer numbers are coprime.
--        Two numbers are coprime if their greatest common divisor
--        equals 1.

areCoprime :: Integral a => a -> a -> Bool
areCoprime x y = gcd x y == 1

prop_33 :: Int -> Int -> Bool
prop_33 x y = areCoprime x y == (gcd x y == 1)


-- ------------------------
-- 34 (**) Calculate Euler's totient function phi(m).
--         (how many integers r (1 <= r < m) are coprime to m?)

totient :: Integral a => a -> Int
totient 1 = 1
totient x = length $ filter (areCoprime x) [1..(x-1)]

prop_34 :: Bool
prop_34 = totient 10 == 4


-- --------------------------------
-- 35 (**) Determine the prime factors of a given positive integer.
--         Construct a flat list containing the prime factors in
--         ascending order.

primeFactors :: Integral a => a -> [a]
primeFactors 0 = []
primeFactors 1 = []
primeFactors x = f : primeFactors (x' `div` f)
    where
        x' = abs x
        f = smallestFactor x' 2
        smallestFactor x' f
            | f*f > x'        = x'
            | x' `mod` f == 0 = f
            | otherwise       = smallestFactor x' (f+1)


primeFactors'' :: Integral a => a -> [a]
primeFactors'' n = primeFactors' n 2
  where
    primeFactors' n f
      | f*f > n        = [n] -- stop looking, it's a prime!
      | n `mod` f == 0 = f : primeFactors' (n `div` f) f
      | otherwise      = primeFactors' n (f + 1)

prop_35 :: Int -> Property
-- incredibly, the primefactors'' can't work with negatives and says
-- 0 = [0] and 1 = [1]
prop_35 x = x > 1 ==> primeFactors x == primeFactors'' x


-- ------------------------------------
-- 36 (**) Determine the prime factors of a given positive integer.
--         Construct a list containing the prime factors and their
--         multiplicity.

primeFactorsMult :: Integral a => a -> [(a,a)]
primeFactorsMult x = map swap $ rleEncode $ primeFactors x
    where swap (x,y) = (y,x)

primeFactorsMult' :: Integral a => a -> [(a,a)]
primeFactorsMult' = map encode . group . primeFactors
    where encode xs = (head xs, genericLength xs)

prop_36 :: Int -> Bool
prop_36 x = primeFactorsMult x == primeFactorsMult' x


-- -----------------------------
-- 37 (**) Calculate Euler's totient function phi(m) (improved).
--         Let ((p1 m1) (p2 m2) (p3 m3) ...) be the list of prime
--         factors (and their multiplicities) of a given number m. Then
--         phi(m) = (p1 - 1) * p1 ** (m1 - 1) * 
--                  (p2 - 1) * p2 ** (m2 - 1) * 
--                  (p3 - 1) * p3 ** (m3 - 1) * ...

totientImp :: Integral a => a -> a
--totientImp 1 = 1
totientImp x = foldl (\acc (p,m) -> (p-1)*p^(m-1) * acc) 1 $ primeFactorsMult x

totientImp' m = product [(p - 1) * p ^ (c - 1) | (p, c) <- primeFactorsMult' m]

prop_37 :: Int -> Bool
prop_37 x = totientImp x == totientImp' x


-- ----------------------------------------
-- 39 (*) A list of prime numbers in specified range.


primesR :: Integral a => a -> a -> [a]
-- filtering eratosthenes' sieve - no, not actually.
primesR a b = takeWhile (<= b) $ dropWhile (< a) $ sieve [2..]
    where sieve (n:ns) = n:sieve [ x | x <- ns, x `mod` n /= 0 ]

primesR' :: Integral a => a -> a -> [a]
primesR' a b = takeWhile (<= b) $ dropWhile (< a) primesTME

-- faster way to generate primes: tree-merging Eratosthenes sieve
primesTME :: Integral a => [a]
primesTME = 2 : gaps 3 (join [[p*p,p*p+2*p..] | p <- primes'])
  where
    primes' = 3 : gaps 5 (join [[p*p,p*p+2*p..] | p <- primes'])
    join  ((x:xs):t)        = x : union xs (join (pairs t))
    pairs ((x:xs):ys:t)     = (x : union xs ys) : pairs t
    gaps k xs@(x:t) | k==x  = gaps (k+2) t 
                    | True  = k : gaps (k+2) xs
    union (x:xs) (y:ys) = case (compare x y) of 
           LT -> x : union  xs  (y:ys)
           EQ -> x : union  xs     ys 
           GT -> y : union (x:xs)  ys

prop_39 :: Bool
prop_39 = primesR 100 1000 == primesR' 100 1000


-- --------------------------
-- 40 (**) Goldbach's conjecture. Write a predicate to find the
--         two prime numbers that sum up to a given even integer.

goldbach :: Integral a => a -> (a,a)
goldbach x
    | x `mod` 2 /= 0 = error "not an even number"
    | otherwise      = helper x 2
    where
        helper x d = if isPrime d && isPrime (x-d)
                        then (d,x-d)
                        else helper x (d+1)

goldbach' n = head [(x,y) | x <- primesR' 2 (n-2),
                           let y = n-x, isPrime y]

prop_40:: Int -> Property
prop_40 x = x > 3 && x `mod` 2 == 0 ==>
            goldbach x == goldbach' x

-- ------------------------------
-- 41(**) Given a range, print a list of all even numbers and their
--        Goldbach composition. then see how many in 2..3000 have both
--        primes bigger than 50

goldbachList :: Integral a => a -> a -> a -> [(a,a)]
goldbachList a b m = filter (\(a,b) -> min a b >= m)
                            $ map goldbach
                            $ filter even [a..b]

-- I don't test this, booring



----------------------------

main :: IO Bool
main = $(quickCheckAll)
--main = quickCheck prop_03

