{-# LANGUAGE TemplateHaskell #-} -- this is to make $(quickCheckAll) work

module Probs54to60 where

-- ---------------------------------------------
-- http://www.haskell.org/haskellwiki/99_questions/54_to_60
-- ---------------------------------------------

import Test.QuickCheck
import Test.QuickCheck.All


-- ------------------------------------
-- let's define the Tree data type

data Tree a = EmptyTree | Branch a (Tree a) (Tree a)
              deriving (Show, Read, Eq)

leaf :: a -> Tree a
leaf x = Branch x EmptyTree EmptyTree


-- ------------------------------
-- 54A (*) Check whether a given term represents a binary tree

-- not a problem in Haskell

-- ------------------------------
-- 55 (**) Construct completely balanced binary trees, all the possible
--         ones, possibly by backtracking.

-- this is wrong (I just create one)
--cbalTree :: Integral a => a -> Tree Char
--cbalTree 0 = EmptyTree
--cbalTree n = Branch 'x' (cbalTree m) (cbalTree (n-m-1))
    --where m = n `div` 2

cbalTree :: Integral a => a -> [Tree Char]
cbalTree 0 = [EmptyTree]
cbalTree n = [Branch 'x' ltree rtree |
                    let (q,r) = quotRem (n-1) 2,
                    m <- [q..q+r], -- just (n-1)/2 if n is odd
                    ltree <- cbalTree m,
                    rtree <- cbalTree (n-m-1)]


-- -------------------------
-- 56 (**) Write a predicate to check whether a given binary tree is
--         symmetric. Hint: Write a predicate first to check whether
--         one tree is the mirror image of another. don't check contents

isMirror :: Tree a -> Tree a -> Bool
isMirror EmptyTree        EmptyTree        = True
isMirror (Branch _ ll lr) (Branch _ rl rr) =
                            isMirror ll rr && isMirror lr rl
isMirror _                _                = False

isSymmetric :: Tree a -> Bool
isSymmetric x = isMirror x x


-- ------------------------
-- 57 (**) Binary search trees (dictionaries). Use add/3 (???) to
--         construct a binary search tree from a list of integers.

tAdd :: Integral a => a -> Tree a -> Tree a
tAdd x EmptyTree                = leaf x
tAdd x t@(Branch v ltree rtree) = case compare x v of
                GT -> Branch v ltree (tAdd x rtree)
                LT -> Branch v (tAdd x ltree) rtree
                EQ -> t

construct :: Integral a => [a] -> Tree a
construct = foldl (flip tAdd) EmptyTree


-- ---------------------------
-- 58 (**) Generate-and-test paradigm: construct all symmetric,
--         balanced binary trees with a given number of nodes.

symCbalTrees :: Integral a => a -> [Tree Char]
symCbalTrees n
    | n `mod` 2 == 0 = [] -- <-- this won't allow the EmptyTree, wrong?
    | otherwise      = [ t | t <- cbalTree n, isSymmetric t]

-- the following is INCREDIBLY MORE efficient, try:
-- prompt> length $ symCbalTrees 71
symCbalTrees' n = if n `mod` 2 == 0 then [] else 
    [ Branch 'x' t (reverseTree t) | t <- cbalTree (n `div` 2)]
 
reverseTree EmptyTree = EmptyTree
reverseTree (Branch x l r) = Branch x (reverseTree r) (reverseTree l)

prop_58 :: Int -> Bool
prop_58 n = symCbalTrees y == symCbalTrees' y
        where y = abs n


-- -----------------------
-- 59 (**) Construct height-balanced binary trees

-- no way, too hard ..... well the second solution is understandable:
hbaltree :: a -> Int -> [Tree a]
hbaltree x 0 = [EmptyTree]
hbaltree x 1 = [Branch x EmptyTree EmptyTree]
hbaltree x h = [Branch x l r |
        (hl, hr) <- [(h-2, h-1), (h-1, h-1), (h-1, h-2)],
        l <- hbaltree x hl, r <- hbaltree x hr]


-- -----------------------
-- 60 ... yeah sure (actually I thought p59 was like this one, maybe
--        that complicated things a bit)


----------------------------

main :: IO Bool
main = $(quickCheckAll)
--main = quickCheck prop_03

