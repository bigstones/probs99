{-# LANGUAGE TemplateHaskell #-} -- this is to make $(quickCheckAll) work

module Probs01to10 where

-- ---------------------------------------------
-- http://www.haskell.org/haskellwiki/99_questions/1_to_10
-- ---------------------------------------------

import Test.QuickCheck
import Test.QuickCheck.All
import Data.List as List

------------------------------------------
-- 01 (*) Find the last element of a list.

myLast :: [a] -> a
--myLast (x:xs) = if null xs then x else myLast xs

--myLast [x] = x
--myLast (_:xs) = myLast xs

myLast xs = foldl1 (\_ x -> x) xs

prop_01 :: [Int] -> Property
prop_01 xs = length xs >= 1 ==>
    myLast xs == last xs


-- -----------------------------------------------
-- 02 (*) Find the last but one element of a list.

myLastButOne :: [a] -> a
myLastButOne [] = error "No last but one element in list of length < 2"
myLastButOne [x2, _] = x2
myLastButOne (_:xs) = myLastButOne xs

prop_02 :: [Int] -> Property
prop_02 xs = length xs >= 2 ==>
    myLastButOne xs == (last . init) xs


-- ---------------------------------------------------------------------
-- 03 (*) Find the K'th element of a list. The first element in the list
--        is number 1.

elemAt :: [a] -> Int -> a
elemAt (x:_) 1 = x
elemAt (_:xs) k = elemAt xs (k-1)
elemAt _ _ = error "Index out of bounds"

prop_03 :: Bool
--prop_03 xs k = -- TODO: this test gives up, too many discards
    --(length xs >= 1) && (k `elem` [1..(length xs)]) ==>
    --elemAt xs k == xs !! (k-1)
prop_03 = elemAt [1..10::Int] 4 == 4


-- ---------------------------------------------
-- 04 (*) Find the number of elements of a list.

myLength :: [a] -> Int
myLength = foldl (\acc _ -> acc + 1) 0

prop_04 :: [Int] -> Bool
prop_04 xs = myLength xs == length xs

-- ----------------------
-- 05 (*) Reverse a list.

myReverse :: [a] -> [a]
--myReverse [] = []
--myReverse [x] = [x]
--myReverse (x:xs) = myReverse xs ++ [x]

--myReverse xs = foldl (\acc x -> x:acc) [] xs
myReverse = foldl (flip (:)) []

prop_05 :: [Int] -> Bool
prop_05 xs = myReverse xs == reverse xs


-- ------------------------
-- 06 (*) Find out whether a list is a palindrome. A palindrome can be
--        read forward or backward; e.g. (x a m a x).

-- isPalindrome xs = xs == reverse xs

isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome [] = True
isPalindrome [_] = True
isPalindrome [a,b] = a == b
isPalindrome xs = frst == lst && isPalindrome middle
    where
        frst = head xs
        lst = last xs
        middle = (init.tail) xs

prop_06 :: [Int] -> Bool
prop_06 xs = (isPalindrome xs) == (xs == reverse xs)


-- -------------------------
-- 07 (**) Flatten a nested list structure.
-- 
-- We have to define a new data type, because lists in Haskell are
-- homogeneous.

data NestedList a = Elem a | List [NestedList a]

-- TODO: understand the type stuff going on, because I plainly copied
flatten :: NestedList a -> [a]
flatten (Elem x) = [x]
flatten (List (x:xs)) = flatten x ++ flatten (List xs)
flatten (List [])     = []

prop_07 :: Bool
--prop_07 xs = .... TODO: how do I test this???
prop_07 = flatten (List [Elem 1, List [Elem 2, List [Elem 3, Elem 4], Elem 5]]) == ([1,2,3,4,5]::[Int])


-- ----------------------------
-- 08 (**) Eliminate consecutive duplicates of list elements.

compress :: (Eq a) => [a] -> [a]
compress [] = []
compress (x:xs) = x : (compress $ dropWhile (== x) xs )

prop_08 :: [Int] -> Bool
prop_08 xs = compress xs == map head (List.group xs)


-- ----------------------------
-- 09 (**) Pack consecutive duplicates of list elements into
--         sublists. If a list contains repeated elements they
--         should be placed in separate sublists.

-- I almost wrote it as in the official library. I'm a genius.
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack xxs@(x:_) = fstPack : pack rest
    where
        (fstPack, rest) = span (== x) xxs

prop_09 :: (Eq a) => [a] -> Bool
prop_09 xs = pack xs == List.group xs


-- -------------------------------
-- 10 (*) RLE of a list. Use the result of problem P09.

rleEncode :: (Eq a, Integral b) => [a] -> [(b, a)]
--rleEncode xs = map (\x -> (length x, head x)) (pack xs)
rleEncode xs = [(fromIntegral $ length x, head x) | x <- group xs]

prop_10 :: (Eq a) => [a] -> Bool
prop_10 xs = rleEncode xs == map (\x -> (length x, head x)) (pack xs)



--------------------------

main :: IO Bool
main = $(quickCheckAll)
--main = quickCheck prop_03
